# %THEME_NAME% Wordpress Theme

## What is this?

This repos is basically a boilerplate for doing wordpress theme development. It's designed to set up a wordpress site for you with some basic content that you can use to develop a theme.

## What am I supposed to do with it?

1. Clone this repo `git clone https://gitlab.com/vgwco/example_wp_theme.git`
2. Change the name of the site in both wp-title.txt and here in the readme
3. Remove the current remote `git remote rm origin`
4. Add your own origin `git remote add origin git@gitlab.com:you/your_wp_theme_name.git`
5. Commit and start adding your own theme files!

## What if you make changes that I want?

Merge this repo into yours!

1. Add the remote back: `git remote add vgwco_example https://gitlab.com/vgwco/example_wp_theme.git`
2. Fetch it: `git fetch vgwco_example`
3. Merge it: `git merge vgwco_example/master master`
4. Push it: `git push origin master`

## Todo: Update the title with info about the project

## Getting Started

- Install Docker
- Run `yarn start`
- Visit http://localhost:8000/wp-admin to log in with `admin` for the username and `admin` for the password

Need to Reset the db or install new plugins or something?

- Run `yarn reset`

## Making changes

### The Theme

The folder called "theme" is automatically in the right place in the wordpress install.

### Adding plugins

Add a line to the wp-commands.txt file to install and activate it. Probably before the theme is activated.

`plugin install {pluging to add like timber-library} --activate`

### Project title

The project title is automatically set when you run docker-compose up, and it's pulled from the `wp-title.txt` file.

## WP-CLI

Running wp-cli from the bin folder `wp` will run the wp-cli from inside of docker. For example: `./bin/wp config set WP_DEBUG false --raw` will enable wp_debug mode.

Alternately, if you have commands you always want to run, just put them in the wp-commands.txt file.
