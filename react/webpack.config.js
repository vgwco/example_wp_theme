var path = require('path');

var package = require("./package.json")

var BUILD_DIR = path.resolve(__dirname, 'theme');
var APP_DIR = path.resolve(__dirname, 'react');

var ZipPlugin = require('zip-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'assets/bundle.js'
  },
  plugins: [
    new CopyPlugin([
      {
        from: BUILD_DIR + '**/*',
        to: BUILD_DIR + '/[name].[ext]'
      },
      {
        from: BUILD_DIR + '**/style.css',
        to: BUILD_DIR + '/[name].[ext]',
        transform: function(content) {
          return content
            .toString()
            .replace(/Theme Name: .*/g, `Theme Name: ${package.theme.title}`)
            .replace(/Theme URI: .*/g, `Theme URI: ${package.homepage}`)
            .replace(/Author: .*/g, `Author: ${package.author.name}`)
            .replace(/Author URI: .*/g, `Author URI: ${package.author.url}`)
            .replace(/Description: .*/g, `Description: ${package.description}`)
            .replace(/Version: .*/g, `Version: ${package.version}`)
            .replace(/Text Domain: .*/g, `Text Domain: ${package.name}`)
            .replace(/License: .*/g, `License: ${package.license}`)
            .replace(/License URI: .*/g, `License URI: https://opensource.org/licenses/${package.license}`)
            .replace(/Tags: .*/g, `Tags: ${package.keywords}`)
        },
      }
    ]),
    new ZipPlugin({
      path: '..',
      pathPrefix: package.name,
      filename: package.name + '.zip'
    })
  ],
  module : {
    rules : [
      {
        test: /\.m?jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react']
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg|html)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
      {
        test: /\.scss$/,
        loaders: [ 'style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap' ]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  }
};

module.exports = config;
