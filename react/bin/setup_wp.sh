#! /bin/sh

repoDir=$(dirname $0)/..

if [[ ! -e /var/www/html/setup_complete ]]; then
  wp core is-installed && installed="done"
  while [[ -z $installed ]]; do
    wp core install --path=/var/www/html/ --admin_user=admin --title="$(cat $repoDir/wp-title.txt)" --admin_password=admin --url="localhost:8000" --skip-email --admin_email=info@example.com && installed="done"
  done

  grep -v '^ *#' < "${repoDir}/wp-commands.txt" | while IFS= read -r line
  do
    sh -c "$line"
  done
fi

touch /var/www/html/setup_complete

echo Wordpress is set up and installed at http://localhost:8000
echo Username and password are admin
