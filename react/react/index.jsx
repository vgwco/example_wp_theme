import React from 'react';
import {render} from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import IndexRoute from './routes/IndexRoute.jsx'

import './Index.scss'

class App extends React.Component {
  render () {
    return(
      <div>
        <Router>
          <div>
            <Route exact path="/" component={IndexRoute}/>
          </div>
        </Router>
      </div>
    )
  }
}

render(<App/>, document.getElementById('app'));
