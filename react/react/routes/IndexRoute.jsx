import React from 'react'

class IndexRoute extends React.Component {
  render() {
    return (
      <div className='Index'>
        The Index Route!
      </div>
    )
  }
}

export default IndexRoute
