<html>
  <head>
    <meta charset="utf-8">
    <title>Wordpress Site!</title>
    <meta name="description" content="A compendium of keyboard layouts." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div id="app" />
    <script src="<?php echo get_template_directory_uri() ?>/assets/bundle.js" type="text/javascript"></script>
  </body>
</html>
