# WP Theme Generator

This is a set of example themes that can be used to spin up new themes for wordpress.

## Starting a new theme:

- Run `./bin/new_theme theme_name`
- Fill out the questions
- Run `docker-compose up` in the new repo.